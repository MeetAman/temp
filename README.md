Launch the cabal repl using new style `cabal new-repl` or old style `cabal repl`

Observed output - 
```
[1 of 2] Compiling One              ( src/One.hs, interpreted )
[2 of 2] Compiling Two              ( src/Two.hs, interpreted )
Ok, two modules loaded.
*One> :t second

<interactive>:1:1: error: Variable not in scope: second
*One> :t first
first :: [Char] -> [Char]
```
